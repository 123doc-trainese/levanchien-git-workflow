# About the project
There are many great README templates available on GitHub; however, I didn't find one that really suited my needs so I created this enhanced one. I want to create a README template so amazing that it'll be the last one you ever need -- I think this is it.

Here's why:

Your time should be focused on creating something amazing. A project that solves a problem and helps others
You shouldn't be doing the same tasks over and over like creating a README from scratch
You should implement DRY principles to the rest of your life smile
Of course, no one template will serve all projects since your needs may be different. So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue. Thanks to all the people have contributed to expanding this template!

Use the BLANK_README.md to get started.

### Build with
- [ReactJS](https://reactjs.org/)
- [PHP](https://www.php.net/)
- [JavaScript](https://www.javascript.com/)
- [NodeJS](https://nodejs.org/en/)

# Getting Started
This is an example of how you may give instructions on setting up your project locally. To get a local copy up and running follow these simple example steps.

### Prerequisites
This is an example of how to list things you need to use the software and how to install them.
- npm 
    ```
    npm install npm@latest -g
    ```

### Installation
Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services.
1. Get a free API Key at [here](https://example.com)
2. Clone the repo
    ```
    git clone https://github.com/your_username_/Project-Name.git
    ```
3. Install NPM packages
    ```
    npm install
    ```

# Roadmap
- [x] Add change log
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
    - [x] Vietnamese
    - [ ] English
sssssssssssssssssssssssssssssssssssssssss
